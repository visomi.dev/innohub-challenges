/**
 * Function for check if one set is subset of another
 * @param {set} subset - the set for check if is a subset of second set
 * @param {set} superset - the set that will contain the first set
 */
function isSubsetOf (subset, superset) {
  if (!(subset instanceof Set) || !(superset instanceof Set)) return false
  const isSubset = [...subset].every(value => superset.has(value))

  return isSubset
}

/**
 * Function for check if one set is superset of another
 * @param {set} superset - the set for validate if have the subset
 * @param {set} subset - the set subset for validate if is inside the superset
 */
function isSupersetOf (superset, subset) {
  if (!(superset instanceof Set) || !(subset instanceof Set)) return false
  const isSuperset = [...subset].every(value => superset.has(value))

  return isSuperset
}

const setA = new Set([1,2])
const setB = new Set([1,2,3])

console.log(setA, 'is subset of', setB, ':', isSubsetOf(setA, setB))
console.log(setB, 'is subset of', setA, ':', isSubsetOf(setB, setA))

console.log('\n')

console.log(setA, 'is superset of', setB, ':', isSupersetOf(setA, setB))
console.log(setB, 'is superset of', setA, ':', isSupersetOf(setB, setA))
