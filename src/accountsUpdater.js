/**
 * Function for make a logging of account logon
 * @param {Array} accounts - the log of the accounts logon
 * @param {Array} logons - the new data
 */
function updateAccounts (accounts = [], logons = []) {
  if (!Array.isArray(accounts) || !Array.isArray(logons)) return []
  const updatedAccounts = logons.reduce((accum, logon) => {
    const accountIndex = accounts.findIndex(account => account.id === logon.id)

    if (accountIndex !== -1) {
      const lastLogonIsOlder = accounts[accountIndex].lastLogon < logon.datetime
      const [ latestLogon ] = logons
        .filter(currentLogon => currentLogon.id = logon.id)
        .sort((left, right) => right.datetime - left.datetime)

      const name = ((lastLogonIsOlder && logon.name) || (!latestLogon.name) && logon.name)
        ? logon.name
        : accounts[accountIndex].name

      accounts[accountIndex] = {
        name,
        id: accounts[accountIndex].id,
        logonCount: ++accounts[accountIndex].logonCount,
        lastLogon: logon.datetime
      }
    } else {
      accum.push({
        id: logon.id,
        name: logon.name,
        logonCount: 1,
        lastLogon: logon.datetime
      })
    }

    return accum
  }, accounts)

  return updatedAccounts.sort((left, right) => left.id - right.id)
}

const logons = [
  {
      id: 5,
      name: 'Sarah Miller',
      datetime: new Date(2017, 1, 23, 10, 12, 4, 545)
  },
  {
      id: 5,
      name: '',
      datetime: new Date(2017, 1, 25, 10, 12, 4, 545)
  },
  {
      id: 5,
      name: 'Sarah Parker-Miller',
      datetime: new Date(2017, 1, 24, 10, 12, 4, 545)
  }
]

const accounts = [
  {
      id: 21,
      name: 'John Shepherd',
      logonCount: 13,
      lastLogon: new Date(2017, 1, 14, 16, 15, 6, 111)
  }
]

console.log('New logging:', updateAccounts(accounts, logons))
