function coordinateIsAdjacent (x1, y1, x2, y2) {
  const right = x1 + 1 === x2 && y1 === y2
  const left = x1 - 1 === x2 && y1 === y2
  const top = x1 === x2 && y1 - 1 === y2
  const bottom = x1 === x2 && y1 + 1 === y2

  return right || left || top || bottom
}

function mapStrToCoordinates (mapStr) {
  const mapToArray = mapStr.split('\n')

  const coordinates = mapToArray.reduce((accum, line, yIndex) => {
    const lineToArray = line.split('')
    console.log(line)

    const lineCoordinates = lineToArray.map((char, xIndex) => ({
      x: xIndex,
      y: yIndex,
      char
    }))

    return accum.concat(lineCoordinates).sort((left, right) => right.y - left.y)
  }, [])

  return coordinates
}

function groupIslands (islandsCoordinates) {
  const groupedIslands = islandsCoordinates.reduce((groups, coordinate) => {
    const { x: x1, y: y1 } = coordinate

    const group = groups.find(currentGroup => {
      return currentGroup.some(currentCoordinate => {
        const { x: x2, y: y2 } = currentCoordinate
        return coordinateIsAdjacent(x1, y1, x2, y2)
      })
    })

    if (group) group.push(coordinate)
    else groups.push([coordinate])

    return groups
  }, [])

  return groupedIslands
}

/**
 * Function for count the inslands on plain string
 * @param {string} mapStr - the string for count the islands
 * @returns {number}
 */
function countIslands (mapStr) {
  const coordinates = mapStrToCoordinates(mapStr)

  const landsCoordinates = coordinates.filter(point => point.char === '0')
  const groupedIslands = groupIslands(landsCoordinates)

  return groupedIslands.length
}

const mapStr = '00...0\n0...00\n......\n0.0.0.\n0.....'
const mapStr2 = '..000.\n..000.\n..000.\n.0....\n..000.'

console.log(`${countIslands(mapStr)} islands`)
console.log('\n')
console.log(`${countIslands(mapStr2)} islands`)
